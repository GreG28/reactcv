# Docker pour faire du react et autres...
FROM debian:latest
MAINTAINER GreG <gfouillard@cardiweb.com>

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q --fix-missing && apt-get -y -q autoclean && apt-get -y -q autoremove)

EXPOSE 8080

RUN apt-get install -y -q apt-utils curl

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

RUN apt-get install -y -q nodejs npm vim

RUN apt-get update && apt-get install -y -q --fix-missing apt-utils locate

RUN apt-get -y autoclean && apt-get -y autoremove

ENV PATH /usr/local/sbin/bin:$PATH

RUN npm config set prefix /usr/local/sbin

RUN ln -s /usr/bin/nodejs /usr/local/bin/node
RUN ln -s /usr/bin/npm /usr/local/bin/npm

RUN npm install webpack -g

RUN npm install npm-check-updates

COPY ./dev /usr/local/sbin/react-cv

RUN cd /usr/local/sbin/react-cv && npm install

RUN cd /usr/local/sbin/react-cv && npm install --only=dev

RUN cd /usr/local/sbin/react-cv && npm update

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q --fix-missing && apt-get -y -q autoclean && apt-get -y -q autoremove)

WORKDIR /usr/local/sbin/react-cv
