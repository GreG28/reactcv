# ReactCV

Pour construire ce nouveau projet l'on utilise mon précédent projet disponible [ici](https://github.com/GreG28/ReactAtWorkTry/tree/master/reactjsprogram)

## Création de l'environnement

Pour commencer à utiliser le projet vous devez construire l'image docker

```
$> sudo docker build -t react-cv .
```

Se placer dans le répertoire "dev"

```
$> cd dev
```

Ensuite vous pouvez lancez le conteneur

```
$> sudo docker run -it -v `pwd`:/usr/local/sbin/react-cv -p 8080:8080 react-cv
```
