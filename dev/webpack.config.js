var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
});
var CopyWebpackPluginConfig = new CopyWebpackPlugin([
    { from: __dirname + '/app/json', to: __dirname + '/dist/json' }
  ],{
      copyUnmodified: true
    });
var DefinePlugin = new webpack.DefinePlugin({
  'process.env': {
    'NODE_ENV': JSON.stringify('production')
  }
});
module.exports = {
  entry: [
    './app/index.js'
  ],
  output: {
    path: __dirname + '/dist',
    filename: "index_bundle.js"
  },
  module: {
    loaders: [
      {test: /\.js$/, exclude: /node_modules/, loader: "babel-loader"}
    ]
  },
  devServer: {
      historyApiFallback: true
  },
  plugins: [HTMLWebpackPluginConfig, CopyWebpackPluginConfig, DefinePlugin]
};
