"use strict";

import React from 'react';
import ReactRouter, {Router, Route, browserHistory, IndexRoute} from 'react-router';
import Main from '../components/Main';
import Home from '../components/Home';
import Article from '../components/Article';

var routes = (
  <Router history={browserHistory}>
    <Route path='/' component={Main}>
      <IndexRoute component={Home} />
      <Route path='/article' component={Article} />
    </Route>
  </Router>
);

module.exports = routes;
