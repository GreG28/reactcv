"use strict";

import React, { Component } from 'react';
import Header from './Header';
import LeftPart from './LeftPart';
import RightPart from './RightPart';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            videos : []
        };
    }

    render() {
        return (
            <div>
                <Header/>
                <LeftPart>
                    <p className='lead'>Left Part</p>
                </LeftPart>
                <RightPart>
                    <p className='lead'>Right Part</p>
                </RightPart>
            </div>
        )
    }
}
module.exports = Home;
