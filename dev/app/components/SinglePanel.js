"use strict";

import React from 'react';

const SinglePanel = (props) => {
    return (
      <div className="col-sm-12">
        {props.children}
      </div>
    );
}

module.exports = SinglePanel;
