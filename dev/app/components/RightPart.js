"use strict";

import React from 'react';

const RightPart = (props) => {
    return (
      <div className="jumbotron col-sm-4 text-center">
        {props.children}
      </div>
    );
}

module.exports = RightPart;
