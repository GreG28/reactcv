"use strict";

import React, { Component } from 'react';
import Header from './Header';
import SinglePanel from './SinglePanel';

class Article extends Component {
    render() {
        return (
            <div>
                <Header/>
                <SinglePanel>
                    <p>Par ici l'article</p>
                </SinglePanel>
            </div>
        );
    }
}

module.exports = Article;
