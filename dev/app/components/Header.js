"use strict";

import React, { Component } from 'react';
import MainContainer from './MainContainer';

class Header extends Component {
    render() {
        return (
            <MainContainer>
                <h1>Header à définir</h1>
                <p className='lead'>Some fancy motto</p>
            </MainContainer>
        )
    }
}
module.exports = Header;
