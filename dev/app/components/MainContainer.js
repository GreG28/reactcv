"use strict";

import React from 'react';

const MainContainer = (props) => {
    return (
      <div className="jumbotron col-sm-12 text-center">
        {props.children}
      </div>
    );
}

module.exports = MainContainer;
