"use strict";

import React, { Component } from 'react';

class Main extends Component {
    render() {
        return (
          <div className='main-container col-sm-12 col-lg-10 col-lg-offset-1'>
              {this.props.children}
          </div>
        )
    }
}

module.exports = Main;
