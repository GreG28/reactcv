"use strict";

import React from 'react';

const LeftPart = (props) => {
    return (
      <div className="jumbotron col-sm-8 text-center">
        {props.children}
      </div>
    );
}

module.exports = LeftPart;
